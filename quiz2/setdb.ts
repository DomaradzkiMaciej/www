import * as DB from './src/db'
import {Quiz} from './src/public/quizInterfaces'

const quizList: Quiz[] = [{
    id: 1,

    introduction: "Zgadnij poprawną liczbę",

    questions: [
        'Uzupełnij ciąg: 0, 7, 26, __, 124, 215',
        'Ile cyfr 6 napotkasz, wypisując liczby od 1 do 100?',
        'Dopisz brakującą liczbę: 1=5, 2=3, 3=4, 4=6, 5=4, 6=5, 7=__',
        'Uzupełnij ciąg: 1, 3, 6, 8, 16, 18, __, 38',
        'Uzupełnij ciąg: 1, 11, 21, 1211, __, 312211, 13112221',
    ],

    answers: [
        63,
        20,
        6,
        36,
        111221,
    ],

    penalty: [
        5,
        4,
        9,
        21,
        5
    ]
}];

const cleanDB = async (): Promise<void> => {
    await DB.clean(db)
        .then(
            () => console.log('Database cleaned'),
            error => fatal(error)
        );
}

const addUsers = async (): Promise<void> => {
    await Promise.all([
        DB.addUser(db, 'user1', 'user1'),
        DB.addUser(db, 'user2', 'user2')
    ])

    console.log('Users added');
}

const addQuiz = async (): Promise<void> => {
    for (const quiz of quizList) {
        await DB.addQuiz(db, quiz);
    }

    console.log('Quizzes added');
}

const fatal = (error: Error): never => {
    console.log(`ERROR: ${error}`);
    process.exit(1);
}

const db = DB.openDB();

cleanDB()
    .then(addUsers)
    .then(addQuiz)
    .then(() => db.close());