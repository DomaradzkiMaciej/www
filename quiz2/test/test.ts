import {expect} from 'chai';
import {driver} from 'mocha-webdriver';
import {Builder, WebDriver} from 'selenium-webdriver';
import {openDB} from '../src/db';

describe('test', () => {
    it("The user's sessions should be logged out after changing the password", async function () {
        this.timeout(20000);

        const driver1 = driver;
        const driver2 = await new Builder().forBrowser('firefox').build();

        await login(driver1, 'user1', 'user1');
        await login(driver2, 'user1', 'user1');

        await changePassword(driver2, 'user1', 'new');
        await driver2.quit();

        await driver1.get('http://localhost:2020/login');
        // only if the user is logged in he can open http://localhost:2020/login, otherwise he will be redirected
        expect(await driver1.getCurrentUrl()).to.equal('http://localhost:2020/login');

        await login(driver1, 'user1', 'new');
        await changePassword(driver1, 'new', 'user1');
    });

    it("It shouldn't be possible to solve the same quiz twice", async function () {
        this.timeout(20000);

        await login(driver, 'user1', 'user1');

        await driver.get('http://localhost:2020/quiz1');
        expect(await driver.getCurrentUrl()).to.equal('http://localhost:2020/quiz1');

        await driver.find('#start-button').doClick();

        for (let i = 0; i < 4; ++i) {
            await driver.find('#answer').sendKeys('1');
            await driver.find('#next-button').doClick();
        }
        await driver.find('#answer').sendKeys('1');

        await driver.find('#submit-button').doClick();
        await driver.find('#end-button').doClick();
        expect(await driver.getCurrentUrl()).to.equal('http://localhost:2020/');

        await driver.get('http://localhost:2020/quiz1');
        expect(await driver.getCurrentUrl()).to.equal('http://localhost:2020/');

        await driver.get('http://localhost:2020/logout');
    });

    it("Check if question times are corrects", async function () {
        const sleepTimes: number[] = [5, 4, 7, 3, 6];
        this.timeout(40000);

        await login(driver, 'user2', 'user2');

        await driver.get('http://localhost:2020/quiz1');
        expect(await driver.getCurrentUrl()).to.equal('http://localhost:2020/quiz1');

        await driver.find('#start-button').doClick();

        for (let i = 0; i < 4; ++i) {
            await driver.sleep(sleepTimes[i] * 1000);
            await driver.find('#answer').sendKeys('1');
            await driver.find('#next-button').doClick();
        }
        await driver.sleep(sleepTimes[4] * 1000);
        await driver.find('#answer').sendKeys('1');

        await driver.find('#submit-button').doClick();
        await driver.find('#end-button').doClick();
        expect(await driver.getCurrentUrl()).to.equal('http://localhost:2020/');

        const times = await getQuizTimes(1, 'user2')
        for (let i = 0; i < 5; ++i) {
            await expect(sleepTimes[i]).to.be.at.least(times[i] - 1).and.to.be.at.most(times[i] + 1);
        }

        await driver.get('http://localhost:2020/logout');
    });

    const login = async (driver: WebDriver, username: string, password: string) => {
        await driver.get('http://localhost:2020/login');
        await driver.find('#username').sendKeys(username);
        await driver.find('#password').sendKeys(password);
        await driver.find('button[type=submit').doClick();

        expect(await driver.getCurrentUrl()).to.equal('http://localhost:2020/');
    }

    const changePassword = async (driver: WebDriver, oldPassword: string, newPassword: string) => {
        await driver.get('http://localhost:2020/changePassword');
        expect(await driver.getCurrentUrl()).to.equal('http://localhost:2020/changePassword');

        await driver.find('#old-password').sendKeys(oldPassword);
        await driver.find('#new-password-1').sendKeys(newPassword);
        await driver.find('#new-password-2').sendKeys(newPassword);
        await driver.find('button[type=submit').doClick();
        expect(await driver.getCurrentUrl()).to.equal('http://localhost:2020/');
    }

    const getQuizTimes = (quizId: number, username: string): Promise<number[]> => {
        const db = openDB();

        return new Promise((resolve, reject) => {
            db.all(
                    `SELECT questionNr, time
                     FROM usersResponse
                     WHERE quizId = ?
                       AND userId = (
                         SELECT id
                         FROM user
                         WHERE username = ?
                     )
                     ORDER BY questionNr;`,
                [quizId, username],
                (error: Error, responses: any[]) => {
                    if (error) {
                        reject(error);
                        return;
                    }

                    db.close();
                    resolve(responses.map(response => response.time));
                });
        });
    }
});
