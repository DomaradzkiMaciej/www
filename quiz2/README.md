# Quiz Server v2

## Introduction

A server that allows logged in users to solve quizzes and which saves statistics from their solution (the user can see them later). Quiz data, encrypted passwords and sessions are stored in a database.

## Usage

Run 'npm build' to build the server and 'npm start' to start it. 
