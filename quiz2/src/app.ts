import express from 'express';
import cookieParser from 'cookie-parser';
import session from 'express-session';
import {Database} from 'sqlite3';
import connect from 'connect-sqlite3';
import csurf from "csurf";
import path from "path";
import * as DB from './db';

import {Quiz, Result, UserResponse} from './public/quizInterfaces';

const SQLiteStore = connect(session);
const SECRET = 'A6dJ5*hh5AH/;';
const app = express();
const store = new SQLiteStore();
const csrfProtect = csurf({cookie: true});

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.urlencoded({extended: true}));
app.use(express.json());
app.use(cookieParser(SECRET));
app.use(session({
    store,
    secret: SECRET,
    resave: false,
    saveUninitialized: false
}))
app.use(csrfProtect);

app.set('view engine', 'pug');

app.get('/', (req, res) => {
    const db = DB.openDB();

    DB.getUnsolvedQuizzesList(db, req.session.username)
        .then(
            (quizzesId: number[]) => {
                db.close();
                res.render('index', {
                    isLogged: isLogged(req),
                    quizzesId,
                })
            }
        )
        .catch(() => fatal(res, db));
});

app.get('/quiz:quizId(\\d+)', (req, res) => {
    if (!isLogged(req)) {
        res.redirect('/login');
        return;
    }

    const db = DB.openDB();
    const quizId: number = parseInt(req.params.quizId, 10);

    DB.isSolved(db, req.session.username, quizId)
        .then(
            (bool: boolean) => {
                if (bool === true)
                    res.redirect('/');
                else
                    res.render('quiz', {
                        csrfToken: req.csrfToken(),
                        quizId
                    });
            }
        )
        .catch(() => fatal(res, db));
});

app.get('/quiz:quizId(\\d+)/json', (req, res) => {
    if (!isLogged(req)) {
        res.redirect('/login');
        return;
    }

    const db = DB.openDB();
    const quizId: number = parseInt(req.params.quizId, 10);

    DB.getQuiz(db, quizId)
        .then(
            (quiz: Quiz) => {
                const startTime = Date.now()
                res.json({quiz, startTime});
            }
        )
        .catch(() => fatal(res, db));
});

app.post('/quiz:quizId(\\d+)', (req, res) => {
    if (!isLogged(req)) {
        res.redirect('/');
        return;
    }

    const db = DB.openDB();
    const quizId: number = parseInt(req.params.quizId, 10);

    DB.getQuiz(db, quizId)
        .then(
            (quiz: Quiz) => {
                const userAnswers: string[] = req.body.answers;
                const times: number[] = req.body.times;
                const startTime: number = req.body.startTime;
                const totalTime: number = Date.now() - startTime;

                let result = 0;
                const responses: UserResponse[] = [];

                for (let i = 1; i < userAnswers.length; ++i) {
                    responses[i - 1] = {isCorrect: false, questionNr: 0, quizId: 0, time: 0, username: ""};
                    responses[i - 1].username = req.session.username;
                    responses[i - 1].quizId = quizId;
                    responses[i - 1].questionNr = i;
                    responses[i - 1].time = Math.round(Number((totalTime / 1000) * (times[i] / 100)));

                    if (isNaN(responses[i - 1].time)) {
                        res.status(404);
                        res.end();
                    }

                    result += responses[i - 1].time;

                    if (quiz.answers[i] === parseInt(userAnswers[i], 10)) {
                        responses[i - 1].isCorrect = true
                    } else {
                        responses[i - 1].isCorrect = false
                        result += quiz.penalty[i];
                    }
                }

                result = Math.round(result * 100) / 100;

                tryAddResponseAndResults(db,res, req.session.username, quizId, result, responses);

                res.send({responses, result});
            }
        )
        .catch((e) => fatal(res, db));
});

app.get('/results', (req, res) => {
    if (!isLogged(req)) {
        res.redirect('/');
        return;
    }

    const db = DB.openDB();

    DB.getSolvedQuizzesList(db, req.session.username)
        .then(
            (quizzesId: number[]) => {
                db.close();
                res.render('results', {
                    isLogged: true,
                    quizzesId,
                })
            }
        )
        .catch(() => fatal(res, db));
});

app.get('/result:quizId(\\d+)', async (req, res) => {
    if (!isLogged(req))
        res.redirect('/');

    const db = DB.openDB();
    const quizId: number = parseInt(req.params.quizId, 10);

    try {
        const responses: UserResponse[] = await DB.getResponse(db, req.session.username, quizId)
        const quiz: Quiz = await DB.getQuiz(db, quizId);
        const top: Result[] = await DB.getTopResults(db, quizId);
        const average: number[] = await DB.getAvgResponseTime(db, quizId);

        db.close();

        const questions: { answer: number, isCorrect: boolean }[] = [];
        for (let i = 0; i < responses.length; ++i) {
            questions[i] = {answer: quiz.answers[i + 1], isCorrect: responses[i].isCorrect};
        }

        res.render('result', {
            isLogged: true,
            id: quizId,
            questions,
            top,
            average
        })
    } catch (e) {
        fatal(res, db)
    }
});

app.get('/login', (req, res) => {
    if (isLogged(req)) {
        res.redirect('/');
        return;
    }

    res.render('login', {
        csrfToken: req.csrfToken(),
        isLogged: false
    });
});

app.post('/login', (req, res) => {
    const db = DB.openDB();
    const username = req.body.username;
    const password = req.body.password;

    DB.isPasswordCorrect(db, username, password)
        .then(
            (isCorrect: boolean) => {
                db.close();

                if (isCorrect) {
                    req.session.regenerate(error => {
                        if (error)
                            fatal(res);

                        req.session.username = username;
                        res.redirect('/');
                    })
                } else {
                    res.redirect('/login');
                }
            }
        )
        .catch(() => fatal(res, db));
})

app.get('/logout', (req, res) => {
    req.session.destroy(error => {
        if (error)
            fatal(res);

        res.redirect('/');
    });
});

app.get('/changePassword', (req, res) => {
    if (!isLogged(req)) {
        res.redirect('/');
        return;
    }

    res.render('changePassword', {
        csrfToken: req.csrfToken(),
        isLogged: true
    });
});

app.post('/changePassword', (req, res) => {
    const db = DB.openDB();
    const username = req.session.username;
    const oldPassword = req.body['old-password'];
    const newPassword1 = req.body['new-password-1'];
    const newPassword2 = req.body['new-password-2'];

    if (newPassword1 !== newPassword2) {
        res.redirect('/changePassword');
        return;
    }

    DB.changePassword(db, username, oldPassword, newPassword1)
        .then(
            async () => {
                db.close();
                destroySession(req)
                    .then(() => res.redirect('/'))
                    .catch(() => fatal(res));
            }
        )
        .catch(() => fatal(res, db));
});

app.get('/error', (req, res) => {
    res.status(404);
    res.render('error', {
        isLogged: isLogged(req),
        message: 'Error'
    });
});

app.use((req, res) => {
    res.redirect('/error');
});

const isLogged = (req: express.Request<any>): boolean => (req.session.username !== undefined);

const fatal = (res: express.Response, db?: Database): void => {
    if (db !== undefined)
        db.close();

    res.redirect('/error');
}

const destroySession = (req: express.Request<any>): Promise<void> => {
    return new Promise((resolve, reject) => {
        store.db.all(
                `SELECT *
                 FROM sessions`, [], async (error: any, data: any[]) => {
                if (error)
                    reject(error);

                for (const sessionData of data) {
                    const sess = JSON.parse(sessionData.sess);

                    if (sess.username === req.session.username) {
                        await new Promise((res, rej) => {
                            store.db.all(
                                    `DELETE
                                     FROM sessions
                                     WHERE sid = ?`, [sessionData.sid], (e: any) => {
                                    if (e)
                                        rej(e)
                                    else
                                        res();
                                }
                            );
                        }).catch((e) => reject(e));

                        resolve();
                    }
                }
            }
        );
    });
}

const tryAddResponseAndResults = (db: Database, res: express.Response, username: string, quizId: number, result: number,
                                  responses: UserResponse[], triesNumber: number = 0) => {
    if (triesNumber === 3) {
        throw new Error("Attempt limit exceeded");
    }

    DB.addResponseAndResults(db, username, quizId, result, responses)
        .then(() => db.close())
        .catch(() => sleep(10)
            .then(() => tryAddResponseAndResults(db, res, username, quizId, result, responses, triesNumber+1))
        );

}

const sleep = (timeInMs: number) => {
    return new Promise((resolve) => {
        setTimeout(resolve, timeInMs);
    });
}

process.on('SIGINT', () => {
    process.exit();
});

export default app;
