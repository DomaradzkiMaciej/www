import * as DB from 'sqlite3';
import crypto from 'crypto';
import {Quiz, Result, UserResponse} from './public/quizInterfaces';

export const DB_NAME = 'quizDB.sqlite3';

export const openDB = (): DB.Database => {
    return new DB.Database(DB_NAME, (error: Error) => {
        if (error) {
            console.log("Can not open database, exiting");
            process.exit(1);
        }
    });
};

export const clean = (db: DB.Database): Promise<void> => {
    return new Promise((resolve, reject) => {
        db.exec(
                `DROP TABLE IF EXISTS user;
            DROP TABLE IF EXISTS quizData;
            DROP TABLE IF EXISTS quizIntro;
            DROP TABLE IF EXISTS usersResult;
            DROP TABLE IF EXISTS usersResponse;
            
            CREATE TABLE user (
                id number PRIMARY KEY,
                username text NOT NULL UNIQUE,
                hash text NOT NULL
            );
            
            CREATE TABLE quizData (
                quizId integer NOT NULL,
                questionNr integer NOT NULL,
                question text NOT NULL,
                answer integer NOT NULL,
                penalty integer NOT NULL,
                PRIMARY KEY(quizId, questionNr)
            );
            
            CREATE TABLE quizIntro (
                quizId integer PRIMARY KEY,
                introduction text NOT NULL
            );
            
            CREATE TABLE usersResult (
                userId integer REFERENCES user(id),
                quizId integer,
                result integer,
                PRIMARY KEY(userId, quizId) 
            );
            
            CREATE TABLE usersResponse (
                userId integer REFERENCES user(id),
                quizId integer,
                questionNr integer,
                time integer,
                isCorrect integer,
                PRIMARY KEY(userId, quizId, questionNr) 
            );`,
            (error: Error) => {
                if (error) {
                    reject(error);
                    return;
                }

                resolve();
            }
        );
    });
}

export const addUser = (db: DB.Database, username: string, password: string): Promise<void> => {
    return new Promise((resolve, reject) => {
        const hash: string = crypto.createHash('sha256').update(password).digest('hex');

        db.run(
                `INSERT INTO user
            VALUES((SELECT IFNULL(MAX(id), 0)+1 FROM user), ?, ?);`,
            [username, hash], (error: Error) => {
                if (error) {
                    reject(error);
                    return;
                }

                resolve();
            }
        );
    });
}

export const isPasswordCorrect = (db: DB.Database, username: string, password: string): Promise<boolean> => {
    return new Promise((resolve, reject) => {
        const hash: string = crypto.createHash('sha256').update(password).digest('hex');

        db.get(
                `SELECT hash FROM user
             WHERE username = ?;`,
            [username], (error: Error, user: { hash: string; }) => {
                if (error) {
                    reject(error);
                    return;
                }

                if (user === undefined || user.hash !== hash) {
                    resolve(false);
                    return;
                }

                resolve(true);
            }
        );
    });
}

export const changePassword = (db: DB.Database, username: string, oldPassword: string, newPassword: string): Promise<boolean> => {
    return new Promise((resolve, reject) => {
        isPasswordCorrect(db, username, oldPassword).then((isCorrect: boolean) => {
            if (!isCorrect)
                resolve(false);

            const hash: string = crypto.createHash('sha256').update(newPassword).digest('hex');

            db.run(
                    `UPDATE user
                SET hash = ?
                WHERE username = ?;`,
                [hash, username], (error: Error) => {
                    if (error) {
                        reject(error);
                        return;
                    }

                    resolve(true);
                }
            );
        });
    });
}

export const addQuiz = (db: DB.Database, quiz: Quiz): Promise<void> => {
    return new Promise((resolve, reject) => {
        if (quiz.questions.length !== quiz.answers.length || quiz.questions.length !== quiz.penalty.length)
            reject(new Error('incorrect quiz'));

        db.exec('BEGIN TRANSACTION', (error: Error) => {
            if (error) {
                reject(error);
                return;
            }
        });

        db.run(
                `INSERT OR ROLLBACK INTO quizIntro
            VALUES(?, ?);`,
            [quiz.id, quiz.introduction], (error: Error) => {
                if (error) {
                    reject(error);
                    return;
                }
            }
        );

        for (let i = 0; i < quiz.answers.length; ++i) {
            db.run(
                    `INSERT OR ROLLBACK INTO quizData
                VALUES(?, ?, ?, ?, ?);`,
                [quiz.id, i + 1, quiz.questions[i], quiz.answers[i], quiz.penalty[i]], (error: Error) => {
                    if (error) {
                        reject(error);
                        return;
                    }
                }
            );
        }

        db.exec('COMMIT', (error: Error) => {
            if (error) {
                reject(error);
                return;
            }

            resolve();
        });
    });
}

export const getQuiz = (db: DB.Database, id: number): Promise<Quiz> => {
    return new Promise((resolve, reject) => {
        const quiz: Quiz = {answers: [], id, introduction: "", penalty: [], questions: []};

        db.get(
                `SELECT introduction
            FROM quizIntro
            WHERE quizId = ?;`,
            [id], (error: Error, data: { introduction: string }) => {
                if (error) {
                    reject(error);
                    return;
                }

                quiz.introduction = data.introduction;
            }
        );

        db.all(
                `SELECT *
            FROM quizData
            WHERE quizId = ?;`,
            [id], (error: Error, quizData) => {
                if (error) {
                    reject(error);
                    return;
                }

                for (const data of quizData) {
                    quiz.questions[data.questionNr] = data.question;
                    quiz.answers[data.questionNr] = data.answer;
                    quiz.penalty[data.questionNr] = data.penalty;
                }

                resolve(quiz);
            }
        );
    });
}

export const getQuizzesList = (db: DB.Database): Promise<number[]> => {
    return new Promise((resolve, reject) => {
        db.all(
                `SELECT quizId
            FROM quizIntro`,
            [], (error: Error, quizzesList: any[]) => {
                if (error) {
                    reject(error);
                    return;
                }

                resolve(quizzesList.map(quiz => quiz.quizId));
            }
        );
    });
}

export const getUnsolvedQuizzesList = (db: DB.Database, username: string): Promise<number[]> => {
    if (username === undefined)
        return getQuizzesList(db);

    return new Promise((resolve, reject) => {
        db.all(
                `SELECT quizIntro.quizId AS id, uQuizId
            FROM quizIntro
            LEFT JOIN (
                SELECT quizId AS uQuizId
                FROM usersResult
                WHERE userId = (
                    SELECT id
                    FROM user
                    WHERE username = ?
                )
            )
            ON quizIntro.quizId = uQuizId
            WHERE uQuizId IS NULL;`,
            [username], (error: Error, quizzesList: any[]) => {

                if (error) {
                    reject(error);
                    return;
                }

                resolve(quizzesList.map(quiz => quiz.id));
            }
        );
    });
}

export const isSolved = (db: DB.Database, username: string, quizId: number): Promise<boolean> => {
    return new Promise((resolve, reject) => {
        db.all(
                `SELECT *
            FROM usersResult
            WHERE userId = (
                SELECT id
                FROM user
                WHERE username = ?
            )
            AND quizId = ?;`,
            [username, quizId], (error: Error, result: any[]) => {

                if (error) {
                    reject(error);
                    return;
                }

                if (result === undefined || result.length === 0)
                    resolve(false);
                else
                    resolve(true);
            }
        );
    });
}

export const addResult = (db: DB.Database, username: string, quizId: number, result: number) => {
    return new Promise((resolve, reject) => {
        db.run(
                `INSERT INTO usersResult
            VALUES((SELECT id FROM user WHERE username = ?), ?, ?);`,
            [username, quizId, result], (error: Error) => {
                if (error) {
                    reject(error);
                    return;
                }

                resolve();
            }
        );
    });
}

export const getTopResults = (db: DB.Database, quizId: number, limit: number = 5): Promise<Result[]> => {
    return new Promise((resolve, reject) => {
        db.all(
                `SELECT username, result
            FROM (SELECT * 
                FROM usersResult
                WHERE quizId = ?
                ORDER BY result DESC
                LIMIT ?)
            LEFT JOIN user
            ON userId = user.id;`,
            [quizId, limit], (error: Error, data) => {
                if (error) {
                    reject(error);
                    return;
                }

                resolve(data);
            }
        );
    });
}

export const getSolvedQuizzesList = (db: DB.Database, username: string): Promise<number[]> => {
    return new Promise((resolve, reject) => {
        db.all(
                `SELECT quizId
            FROM usersResult
            where userId = (SELECT id
                 FROM user
                 WHERE username = ?)`,
            [username], (error: Error, resultsList: any[]) => {
                if (error) {
                    reject(error);
                    return;
                }

                resolve(resultsList.map(result => result.quizId));
            }
        );
    });
}

export const addResponse = (db: DB.Database, response: UserResponse): Promise<void> => {
    return new Promise((resolve, reject) => {
        db.run(
                `INSERT INTO usersResponse
            VALUES((SELECT id FROM user WHERE username = ?), ? ,?, ?, ?);`,
            [response.username, response.quizId, response.questionNr, response.time, response.isCorrect ? 1 : 0],
            (error: Error) => {
                if (error) {
                    reject(error);
                    return;
                }

                resolve();
            }
        );
    });
}

export const getResponse = (db: DB.Database, username: string, quizId: number): Promise<UserResponse[]> => {
    return new Promise((resolve, reject) => {
        db.all(
                `SELECT *
            FROM usersResponse
            WHERE userId = (
                SELECT id
                FROM user
                WHERE username = ?
            )
            AND quizId = ?
            ORDER BY questionNr ASC;`,
            [username, quizId], (error: Error, response: UserResponse[]) => {

                if (error) {
                    reject(error);
                    return;
                }

                resolve(response);
            }
        );
    });
}

export const getAvgResponseTime = (db: DB.Database, quizId: number): Promise<number[]> => {
    return new Promise((resolve, reject) => {
        db.all(
                `SELECT questionNr, AVG(time) as avg
            FROM usersResponse WHERE quizId = ?
            GROUP BY questionNr
            ORDER BY questionNr ASC;`,
            [quizId],
            (error: Error, data: any[]) => {
                if (error) {
                    reject(error);
                    return;
                }

                resolve(data.map(average => Math.round(average.avg * 100) / 100));
            });
    });
}

export const addResponseAndResults = (db: DB.Database, username: string, quizId: number, result: number,
                                      responses: UserResponse[]): Promise<void> => {
    return new Promise(async (resolve, reject) => {
        db.exec('BEGIN TRANSACTION', (error: Error) => {
            if (error) {
                reject(error);
                return;
            }
        });

        await addResult(db, username, quizId, result);

        for (const response of responses) {
            await addResponse(db, response);
        }

        db.exec('COMMIT', (error: Error) => {
            if (error) {
                reject(error);
                return;
            }

            resolve();
        });
    });
}