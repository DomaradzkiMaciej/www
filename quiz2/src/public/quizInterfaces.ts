export interface Quiz {
    id: number,
    introduction: string,
    questions: string[],
    answers: number[],
    penalty: number[]
}

export interface Result {
    username: string,
    result: number
}

export interface UserResponse {
    username: string,
    quizId: number,
    questionNr: number,
    time: number,
    isCorrect: boolean
}