import {Quiz, UserResponse} from './quizInterfaces';
import Timeout = NodeJS.Timeout;

let quizJson: Quiz;
let interval: Timeout;
let answerCounter: number = 0;
let currentQuestion: number = 1;
const answers: string[] = [];
let times: number[] = [];
let startTime: number;
let previousTime: number = 0;
let currentTime: number = 0;

const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

const timer = document.querySelector('#timer') as HTMLDivElement;
const cancelButton = document.querySelector('#cancel-button') as HTMLButtonElement;
const endButton = document.querySelector('#end-button') as HTMLButtonElement;

const introduction = document.querySelector('#introduction') as HTMLDivElement;

const start = document.querySelector('#start') as HTMLDivElement;
const startButton = document.querySelector('#start-button') as HTMLButtonElement;

const quiz = document.querySelector('#quiz') as HTMLDivElement;
const question = document.querySelector('#question') as HTMLDivElement;
const answerInput = document.querySelector('#answer') as HTMLInputElement;

const navigationButtons = document.querySelector('#navigation-buttons') as HTMLDivElement
const previousButton = document.querySelector('#previous-button') as HTMLButtonElement;
const submitButton = document.querySelector('#submit-button') as HTMLButtonElement;
const nextButton = document.querySelector('#next-button') as HTMLButtonElement;

const timeCounter = (): { next: () => number } => {
    let i = 0;

    const next = (): number => {
        i++;
        return i;
    }

    return {next};
}

const init = (): void => {
    quiz.style.display = 'none';
    navigationButtons.style.display = 'none';
    cancelButton.style.display = 'none';
    endButton.style.display = 'none';
}

const saveTime = (): void => {
    currentTime = parseInt(timer.innerText, 10);

    if (times[currentQuestion] === undefined)
        times[currentQuestion] = 0;

    times[currentQuestion] += currentTime - previousTime;
    previousTime = currentTime;
}

const setQuestion = (): void => {
    answerInput.value = (answers[currentQuestion] === undefined) ? '' : answers[currentQuestion];
    previousButton.disabled = currentQuestion === 1;
    nextButton.disabled = currentQuestion === quizJson.questions.length - 1;
    question.innerText = `${currentQuestion}/${quizJson.questions.length - 1}\n`
        + `${quizJson.questions[currentQuestion]}\n`
        + `Kara za złą odpowiedź: ${quizJson.penalty[currentQuestion]}`
}

const changeQuestion = (i: number): void => {
    saveTime();
    currentQuestion += i;
    setQuestion();
}

const startQuiz = (): void => {
    start.style.display = 'none';
    cancelButton.style.display = 'initial';
    console.log(quizJson.introduction);
    introduction.innerText = quizJson.introduction;
    quiz.style.display = 'grid'
    navigationButtons.style.display = 'flex';

    submitButton.disabled = true;
    timer.innerText = '0';

    const tic = timeCounter();

    interval = setInterval(() => {
        timer.innerText = `${tic.next()}`;
    }, 1000)

    setQuestion();
}

const getAnswer = (): void => {
    if (isNaN(parseInt(answerInput.value, 10))) {
        alert("answer must be a number");
        answerInput.value = '';
        return
    }

    if (answers[currentQuestion] === undefined)
        answerCounter += 1;
    else if (answerInput.value === '')
        answerCounter -= 1;

    answers[currentQuestion] = (answerInput.value === '') ? undefined : answerInput.value;
    submitButton.disabled = answerCounter !== quizJson.questions.length - 1;
}

const finish = (): void => {
    saveTime();
    clearInterval(interval);

    let fullTime = 0
    times.forEach(time => fullTime += time);
    times = times.map((time) => time / fullTime * 100);

    fetch(`quiz${quizJson.id}`, {
        credentials: 'same-origin',
        headers: {
            'Content-type': 'application/json',
            'CSRF-Token' : csrfToken
        },
        method: 'POST',
        body: JSON.stringify({
            answers,
            times,
            startTime
        })
    }).then(async (response) => {
            if (!response.ok)
                location.href = '/'

            quiz.style.display = 'none';
            cancelButton.style.display = 'none';
            endButton.style.display = 'initial';
            navigationButtons.style.display = 'none';
            timer.style.display = 'none';

            let text: string = "";
            const res = await response.json();
            const responses: UserResponse[] = res.responses;

            for (let i = 0; i < responses.length; i++) {
                text += (i + 1) + " ";

                if (responses[i].isCorrect)
                    text += "correct " + responses[i].time;
                else
                    text += "incorrect " + responses[i].time + " + " + quizJson.penalty[i+1];

                text += "\n"
            }
            text += "\nYour results: " + res.result;
            introduction.innerText = text;
        }
    );
}

init();

fetch(`${window.location.href}/json`)
    .then(async (response) => {
        if (!response.ok)
            location.href = '/';

        const json = await response.json();

        quizJson = json.quiz;
        startTime = json.startTime;

        startButton.addEventListener('click', startQuiz);
        answerInput.addEventListener('input', getAnswer);
        answerInput.addEventListener('keypress', (event: KeyboardEvent) => {
            if (event.key === 'Enter') {
                event.preventDefault();
            }
        });
        cancelButton.addEventListener('click', () => location.href = '/');
        nextButton.addEventListener('click', () => changeQuestion(1));
        previousButton.addEventListener('click', () => changeQuestion(-1));
        submitButton.addEventListener('click', finish);
        endButton.addEventListener('click', () => location.href = '/');
    })
    .catch(() => location.href = '/');
