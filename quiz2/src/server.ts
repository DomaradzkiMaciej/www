import app from './app'

app.listen(2020, () => {
    console.log('App is running at http://localhost:2020/');
    console.log('Press Ctrl+C to stop');
});