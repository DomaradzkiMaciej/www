import express from 'express';
import cookieParser from 'cookie-parser';
import csurf from 'csurf';
import session from 'express-session';
import {Database} from 'sqlite3';
import connect from 'connect-sqlite3'

import * as DB from './db';
import {Meme, MemeWithPriceHistory} from './memeInterface'

const SQLiteStore = connect(session);
const SECRET = 'AJTjiaYH77';
const app = express();
const csrfProtect = csurf({cookie: true});

app.use(express.urlencoded({extended: true}));
app.use(cookieParser(SECRET));
app.use(session({
    store: new SQLiteStore(),
    secret: SECRET,
    resave: false,
    saveUninitialized: false
}))

app.set('view engine', 'pug');

app.get('/', (req, res) => {
    const db = DB.openDB();

    DB.getThreeMostExpensiveMemes(db)
        .then(
            (memes: Meme[]) => {
                db.close();
                res.render('index', {
                    title: 'Meme market',
                    message: 'Hello there!',
                    logged: isLogged(req),
                    memes
                })
            }
        )
        .catch(() => fatal(res, db));
});

app.get('/meme/:memeId(\\d+)', csrfProtect, (req, res) => {
    const db = DB.openDB();
    const memeId: number = parseInt(req.params.memeId, 10);

    DB.getMemeByID(db, memeId)
        .then(
            (meme: MemeWithPriceHistory) => {
                db.close();
                res.setHeader('CSRF-Header', req.csrfToken());
                res.render('meme', {
                    logged: isLogged(req),
                    meme,
                    csrfToken: req.csrfToken()
                })
            }
        )
        .catch(() => fatal(res, db));
})

app.post('/meme/:memeId(\\d+)', csrfProtect, (req, res) => {
    if (!isLogged(req)) {
        res.redirect('/error');
        return;
    }

    const db = DB.openDB();
    const username = req.session.username;
    const memeId: number = parseInt(req.params.memeId, 10);
    const price: number = Number(req.body.price);

    if (req.body.price !== '' && !isNaN(price) && price >= 0)
        addMemePrice(db, res, memeId, price, username);

    res.redirect(303, `/meme/${memeId}`);
})

app.get('/login', (req, res) => {
    if (isLogged(req)) {
        res.redirect('/');
        return;
    }

    res.render('login', {logged: false});
});

app.post('/checkUser', (req, res) => {
    const db = DB.openDB();
    const username = req.body.username;
    const password = req.body.password;

    DB.checkIfUserExist(db, username, password)
        .then(
            exist => {
                db.close();

                if (exist) {
                    req.session.regenerate(error => {
                        if (error)
                            fatal(res);

                        req.session.username = username;
                        res.redirect('/');
                    })
                } else {
                    res.redirect('/login');
                }
            }
        )
        .catch(() => fatal(res, db));
})

app.get('/logout', (req, res) => {
    req.session.destroy(error => {
        if (error)
            fatal(res);

        res.redirect('/');
    });
});

app.get('/error', (req, res) => {
    res.status(404);
    res.render('error', {logged: isLogged(req)});
});

app.use((req, res) => {
    res.redirect('/error');
});

app.listen(1500, () => {
    console.log('App is running at http://localhost:1500/');
    console.log('Press Ctrl+C to stop');
});

const isLogged = (req: express.Request<any>): boolean => req.session.username;

const fatal = (res: express.Response, db?: Database): void => {
    if (db !== undefined)
        db.close();

    res.redirect('/error');
}

const addMemePrice = (db: Database, res: express.Response, memeId: number, price: number, username: string,
                      triesNumber: number = 0) => {
    if (triesNumber === 3) {
        fatal(res, db);
        return;
    }

    DB.addPrice(db, memeId, price, username)
        .then(() => db.close())
        .catch(() => sleep(10)
            .then(() => addMemePrice(db, res, memeId, price, username, triesNumber + 1))
        );
}

const sleep = (timeInMs: number) => {
    return new Promise((resolve) => {
        setTimeout(resolve, timeInMs);
    });
}