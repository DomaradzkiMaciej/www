import * as DB from 'sqlite3';
import crypto from 'crypto';
import {Meme, MemeWithPriceHistory, isMeme} from './memeInterface';

export const DB_NAME = 'memesDB.sqlite3';

export const openDB = (): DB.Database => {
    return new DB.Database(DB_NAME, (error: Error) => {
        if (error) {
            console.log("Can not open database, exiting");
            process.exit(1);
        }
    });
};

export const clean = (db: DB.Database): Promise<void> => {
    return new Promise((resolve, reject) => {
        db.exec(
                `DROP TABLE IF EXISTS user;
            DROP TABLE IF EXISTS meme;
            DROP TABLE IF EXISTS memesPrice;
            
            CREATE TABLE user (
                username text PRIMARY KEY,
                hash text NOT NULL
            );
            
            CREATE TABLE meme (
                id integer PRIMARY KEY,
                name text NOT NULL,
                url text NOT NULL,
                currentPrice integer NOT NULL,
                whoSet text NOT NULL
            );
            
            CREATE TABLE memesPrice (
                id integer PRIMARY KEY,
                price integer NOT NULL,
                memeId integer NOT NULL REFERENCES meme(id),
                whoSet text NOT NULL REFERENCES user(username),
                CONSTRAINT price_check CHECK (price >= 0)
            );`,
            (error: Error) => {
                if (error) {
                    reject(error);
                    return;
                }

                resolve();
            });
    });
}

export const checkIfUserExist = (db: DB.Database, username: string, password: string): Promise<boolean> => {
    return new Promise((resolve, reject) => {
        const hash: string = crypto.createHash('sha256').update(password).digest('hex');

        db.get(
                `SELECT hash FROM user
            WHERE username = ?;`,
            [username], (error: Error, user: { hash: string; }) => {
                if (error) {
                    reject(error);
                    return;
                }

                if (user === undefined || user.hash !== hash) {
                    resolve(false);
                    return;
                }

                resolve(true);
            });
    });
}

export const addUser = (db: DB.Database, username: string, password: string): Promise<void> => {
    return new Promise((resolve, reject) => {
        const hash: string = crypto.createHash('sha256').update(password).digest('hex');

        db.run(
                `INSERT INTO user
            VALUES(?, ?);`,
            [username, hash], (error: Error) => {
                if (error) {
                    reject(error);
                    return;
                }

                resolve();
            });
    });
}

export const addMeme = (db: DB.Database, meme: Meme | MemeWithPriceHistory, username: string): Promise<void> => {
    return new Promise((resolve, reject) => {
        const price: number = (isMeme(meme)) ? meme.price : meme.prices[meme.prices.length - 1];

        db.run(
            `INSERT INTO meme
            VALUES(?, ?, ?, ?, ?);`,
            [meme.id, meme.name, meme.url, price, username], (error: Error) => {
                if (error) {
                    reject(error);
                    return;
                }

                resolve();
            });
    });
}

export const getMemeByID = (db: DB.Database, memeId: number): Promise<MemeWithPriceHistory> => {
    return new Promise((resolve, reject) => {
        db.all(
            `SELECT meme.id AS id, name, url, IFNULL(price, 0) AS price 
            FROM meme
            LEFT JOIN memesPrice
            ON meme.id = memeId
            WHERE meme.id = ?
            ORDER BY memesPrice.id`,
            [memeId], (error: Error, memeData: Meme[]) => {
                if (error) {
                    reject(error);
                    return;
                }

                if (memeData.length === 0) {
                    reject(Error("Meme not found"));
                    return;
                }

                const meme: MemeWithPriceHistory = {
                    id: memeData[0].id,
                    name: memeData[0].name,
                    url: memeData[0].url,
                    prices: []
                };

                for (const data of memeData) {
                    meme.prices.push(data.price);
                }

                resolve(meme);
            }
        );
    });
}

export const getThreeMostExpensiveMemes = (db: DB.Database): Promise<Meme[]> => {
    return new Promise((resolve, reject) => {
        db.all(
                `SELECT id, name, url, IFNULL(currentPrice, 0) AS price 
            FROM meme
            ORDER BY price DESC
            LIMIT 3;`
            , (error: Error, memes) => {
                if (error) {
                    console.log(error);
                    reject(error);
                    return;
                }

                resolve(memes);
            }
        );
    });
}

export const addPrice = (db: DB.Database, memeId: number, price: number, username: string): Promise<void> => {
    return new Promise((resolve, reject) => {
        db.exec('BEGIN IMMEDIATE', (error: Error) => {
            if (error) {
                reject(error);
                return;
            }
        });

        db.run(
            `UPDATE meme
            SET currentPrice = ?, whoSet = ?
            WHERE id = ?;`,
            [price, username, memeId], (error: Error) => {
                if (error) {
                    reject(error);
                    return;
                }
            });

        db.run(
            `INSERT INTO memesPrice
            VALUES((SELECT IFNULL(MAX(id), 0)+1 FROM memesPrice), ?, ?, ?);`,
            [price, memeId, username], (error: Error) => {
                if (error) {
                    reject(error);
                    return;
                }
            });

        db.exec('COMMIT', (error: Error) => {
            if (error) {
                reject(error);
                return;
            }
            resolve();
        });
    });
}
