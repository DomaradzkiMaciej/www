export interface Meme {
    id: number;
    name: string;
    url: string;
    price: number;
}

export interface MemeWithPriceHistory {
    id: number;
    name: string;
    url: string;
    prices: number[];
}

export const isMeme = (object: any): object is Meme =>{
  return object.hasOwnProperty('id') && object.hasOwnProperty('name')
      && object.hasOwnProperty('url') && object.hasOwnProperty('price');
};