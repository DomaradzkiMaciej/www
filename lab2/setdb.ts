import * as DB from './src/db'

const mostExpensive = [
    {
        'id': 10,
        'name': 'Gold',
        'prices': [1000, 900, 500],
        'url': 'https://i.redd.it/h7rplf9jt8y21.png'
    },
    {
        'id': 9,
        'name': 'Platinum',
        'prices': [1100, 1500],
        'url': 'http://www.quickmeme.com/img/90/90d3d6f6d527a64001b79f4e13bc61912842d4a5876d17c1f011ee519d69b469.jpg'
    },
    {
        'id': 8,
        'name': 'Elite',
        'prices': [1200],
        'url': 'https://i.imgflip.com/30zz5g.jpg'
    },
    {
        'id': 7,
        'name': 'Very rare',
        'prices': [1300, 1000],
        'url': 'https://i.redd.it/qsfso5ou26o31.jpg'
    }
];

const cleanDB = async (): Promise<void> => {
    await DB.clean(db)
        .then(
            () => console.log('Database cleaned'),
            error => fatal(error)
        );
}

const addUsers = async (): Promise<void> => {
    await Promise.all([
        DB.addUser(db, 'admin', 'admin'),
        DB.addUser(db, 'user', 'user')
    ])

    console.log('Users added');
}

const addMemes = async (): Promise<void> => {
    for (const meme of mostExpensive) {
        await DB.addMeme(db, meme, 'admin');

        for (const price of meme.prices)
            await DB.addPrice(db, meme.id, price, 'admin');
    }

    console.log('Memes added');
}

const fatal = (error: Error): never => {
    console.log(`ERROR: ${error}`);
    process.exit(1);
}

const db = DB.openDB();

cleanDB()
    .then(addUsers)
    .then(addMemes)
    .then(() => db.close());

