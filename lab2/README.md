# Meme Server

## Introduction

The server serves a page with memes and allows logged in users to change their prices. Meme data, encrypted passwords and sessions are stored in a database.
## Usage

Run 'npm build' to build the server and 'npm start' to start it. 
