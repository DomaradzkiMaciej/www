# Quiz Server v1

## Introduction

A page with a quiz that gives the result after the approach. 
The site gives the time from the start of the approach and does not allow its completion until all the answers are given.

## Usage

Run 'npm run compile' to build the server then open quiz.html. 
