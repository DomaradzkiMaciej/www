const quizJSON = {
    id: 1,

    questions: [
        'Uzupełnij ciąg: 0, 7, 26, __, 124, 215',
        'Ile cyfr 6 napotkasz, wypisując liczby od 1 do 100?',
        'Dopisz brakującą liczbę: 1=5, 2=3, 3=4, 4=6, 5=4, 6=5, 7=__',
        'Uzupełnij ciąg: 1, 3, 6, 8, 16, 18, __, 38',
        'Uzupełnij ciąg: 1, 11, 21, 1211, __, 312211, 13112221',
    ],

    answers: [
        63,
        20,
        6,
        36,
        111221,
    ],
}

// header related elements
const cancelButton = document.querySelector('#cancel-button') as HTMLButtonElement;

// best result related elements
const manualDiv = document.querySelector('#manual') as HTMLDivElement;

// best result related elements
const bestResultDiv = document.querySelector('#best-result-div') as HTMLDivElement;
const bestResult = document.querySelector('#best-result-header') as HTMLHeadingElement;

// start related elements
const beginning = document.querySelector('#beginning') as HTMLDivElement;
const startButton = document.querySelector('#start-button') as HTMLButtonElement;

// quiz related elements
const quiz = document.querySelector('#quiz') as HTMLDivElement;
const question = document.querySelector('#question') as HTMLHeadingElement;
const notification = document.querySelector('#notification') as HTMLParagraphElement;
const form = document.querySelector('#form') as HTMLFormElement;
const time = document.querySelector('#time') as HTMLParagraphElement;
const questionNumber = document.querySelector('#question-number') as HTMLHeadingElement;
const previousButton = document.querySelector('#previous-button') as HTMLButtonElement;
const finishButton = document.querySelector('#finish-button') as HTMLButtonElement;
const nextButton = document.querySelector('#next-button') as HTMLButtonElement;
const answerInput = document.querySelector('#answer-input') as HTMLInputElement;

// finish related elements
const end = document.querySelector('#end') as HTMLDivElement;
const resultsList = document.querySelector('#results-list') as HTMLUListElement;
const resultHeading = document.querySelector('#results-heading') as HTMLHeadingElement;
const saveOptions = document.querySelector('#save-options') as HTMLDivElement;

let correctness: Boolean[];
let times: number[];
let currentQuestion: number;
let answersCounter: number;
let oldTime: number;
let newTime: number;
let result: number;
let interval: number;
let db: IDBDatabase;


class ResultClass {
    public result: number;
    public correctness: Boolean[];

    constructor() {
    }
}


function openDBAndShowStatistics() {
    let request = window.indexedDB.open('DB', 1);

    request.onupgradeneeded = function () {
        db = request.result;
        db.createObjectStore('statistics', {autoIncrement: true});
        db.createObjectStore('bestResult');
    };
    request.onerror = function () {
        db = null;
    };
    request.onsuccess = function () {
        db = request.result;
        showStatistics();
    };
}

function timeCounter(): { next: () => number } {
    let i = 0;

    function next() {
        i++;
        return i;
    }

    return {next: next};
}

// function shows current question and its number, and hides notification
function showQuestion(): void {
    question.innerText = quizJSON.questions[currentQuestion - 1];
    questionNumber.innerText = `${currentQuestion}/${quizJSON.answers.length}`;
    notification.style.visibility = 'hidden'
}

function saveTime(): void {
    if (times[currentQuestion] == undefined)
        times[currentQuestion] = 0;

    times[currentQuestion] += newTime - oldTime;
    oldTime = newTime;
}

// returns string containing result of i-th question
function getPositionFromResult(position: number): string {
    let text: string = `${position}. `;

    if (correctness[position] === true)
        text += `Dobra odpowiedź`;
    else
        text += `Zła odpowiedź +10s`;

    return text;
}

function makeResultsListElements(list: HTMLUListElement, length: number, getText: (i: number) => string): void {
    for (let i = 1; i <= length; i++) {
        let item = document.createElement('li');
        item.appendChild(document.createTextNode(getText(i)));
        list.appendChild(item);
    }
}

function makeResultParagraph(): HTMLParagraphElement {
    let paragraph = document.createElement('h3');
    result = parseInt(time.innerText);

    for (let i = 1; i <= quizJSON.answers.length; i++) {
        if (correctness[i] === false)
            result += 10;
    }

    paragraph.innerText = `Łącznie: ${result} pkt`;
    return paragraph
}

function saveResult(res: any): void {
    if (db !== null) {
        let objectStore = db.transaction('statistics', 'readwrite').objectStore('statistics');
        objectStore.put(res);
    }
}

function saveBestResult(): void {
    if (db !== null) {
        let objectStore = db.transaction('bestResult', 'readwrite').objectStore('bestResult');
        let request = objectStore.get(0);

        request.onsuccess = function () {
            let best = request.result;

            if (isNaN(best) || result < best) {
                objectStore.put(result, 0);
            }
        };
    }
}

function start(): void {
    beginning.style.display = 'grid';
    quiz.style.display = 'none';
    end.style.display = 'none';
    cancelButton.style.display = 'none';

    if (window.matchMedia('(max-width: 1200px)').matches) {
        manualDiv.style.display = 'initial';
        document.body.style.gridTemplateRows = 'min-content min-content auto 4vh'
    } else {
        bestResultDiv.style.display = 'initial';
    }
}

function showStatistics() {
    if (db !== null) {
        let request = db.transaction('bestResult').objectStore('bestResult').get(0);

        request.onsuccess = function () {
            let best = request.result;

            if (isNaN(best))
                bestResult.innerText = 'Rozwiąż wpierw quiz'
            else
                bestResult.innerText = `${best} pkt`
        };
        request.onerror = function () {
            bestResult.innerText = 'Nie można się połączyć z IndexedDB';
        }
    } else {
        bestResult.innerText = 'Nie można się połączyć z IndexedDB';
    }
}

function startQuiz(): void {
    correctness = [];
    times = [];
    currentQuestion = 1;
    answersCounter = 0;
    oldTime = 0;
    newTime = 0;
    time.innerText = '0';

    quiz.style.display = 'grid';
    cancelButton.style.display = 'flex';
    notification.style.visibility = 'hidden';
    beginning.style.display = 'none';
    bestResultDiv.style.display = 'none';

    if (window.matchMedia('(max-width: 1200px)').matches) {
        manualDiv.style.display = 'none';
        document.body.style.gridTemplateRows = 'min-content 0 auto 4vh'
    }

    finishButton.disabled = true;
    previousButton.disabled = true;
    nextButton.disabled = false;

    form.reset();
    showQuestion();
    const tic = timeCounter();

    interval = setInterval(() => {
        newTime = tic.next();
        time.innerText = `${newTime}`;
    }, 1000);
}

function cancelQuiz(): void {
    clearInterval(interval);
    start();
    showStatistics();
}

function submitForm(event: Event): void {
    event.preventDefault();

    const answer: number = parseInt(answerInput.value);
    if (isNaN(answer)) {
        alert('Odpowiedź powinna być liczbą!');
        return;
    }

    if (correctness[currentQuestion] == undefined) {
        answersCounter++;

        if (answersCounter == quizJSON.questions.length)
            finishButton.disabled = false;
    }

    notification.style.visibility = 'initial';

    correctness[currentQuestion] = (answer === quizJSON.answers[currentQuestion - 1]);
    setTimeout(() => notification.style.visibility = 'hidden', 3000);
    form.reset();
}

function finishQuiz(): void {
    saveTime();
    clearInterval(interval);

    end.style.display = 'grid';
    quiz.style.display = 'none';
    cancelButton.style.display = 'none';
    resultsList.innerHTML = '';
    resultHeading.innerHTML = '';

    makeResultsListElements(resultsList, quizJSON.questions.length, getPositionFromResult);
    resultHeading.appendChild(makeResultParagraph());
}

function previousQuestion(): void {
    saveTime();

    currentQuestion--;
    if (currentQuestion == 1)
        previousButton.disabled = true;
    if (currentQuestion + 1 == quizJSON.questions.length)
        nextButton.disabled = false;

    answerInput.value = '';
    showQuestion();
}

function nextQuestion(): void {
    saveTime();

    currentQuestion++;
    if (currentQuestion == 2)
        previousButton.disabled = false;
    if (currentQuestion == quizJSON.questions.length)
        nextButton.disabled = true;

    answerInput.value = '';
    showQuestion();
}

function saveQuiz(event: Event): void {
    let resultClass = new ResultClass();
    resultClass.result = result;

    if ((event.target as HTMLButtonElement).id === 'save-yes-button') {
        resultClass.correctness = correctness;
    }

    saveResult(resultClass);
    saveBestResult();
    start();
    showStatistics();
}


startButton.addEventListener('click', startQuiz);
cancelButton.addEventListener('click', cancelQuiz);
form.addEventListener('submit', submitForm);
finishButton.addEventListener('click', finishQuiz);
previousButton.addEventListener('click', previousQuestion);
nextButton.addEventListener('click', nextQuestion);
saveOptions.addEventListener('click', saveQuiz);
window.addEventListener('resize', () => {
    setTimeout( () => window.location.reload(),0);
})

start()
openDBAndShowStatistics();
