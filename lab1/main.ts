const reservation = document.querySelector('#flight_reservation') as HTMLFormElement;

type formFields = {
    from: string,
    to: string,
    name: string,
    surname: string,
    date: string
}

function getFormFields(): formFields {
    const from = (document.querySelector('#from') as HTMLOptionElement).value;
    const to = (document.querySelector('#to') as HTMLOptionElement).value;
    const name = (document.querySelector('#name') as HTMLInputElement).value;
    const surname = (document.querySelector('#surname') as HTMLInputElement).value;
    const date = (document.querySelector('#date') as HTMLInputElement).value;

    return {from, to, name, surname, date};
}

function checkData(): boolean {
    const {from, to, name, surname, date} = getFormFields();

    if (!date)
        return false;

    const difference: number = Date.parse(date) + 1000 * 60 * 60 * 24 - Date.now();

    if (!(from && to && name && surname) || difference < 0)
        return false;

    return true;
}

function showFormData() {
    const {from, to, name, surname, date} = getFormFields();
    const notification = document.querySelector('#booked h1') as HTMLInputElement;

    notification.removeAttribute('hidden');
    notification.innerHTML =
        `Skąd: ${from}<br>Dokąd: ${to}<br>Imię: ${name}<br>Nazwisko: ${surname}<br>Data: ${date}`;
}

reservation.addEventListener('input', () => {
    if (checkData())
        document.querySelector('[type=submit]').removeAttribute('disabled');
    else
        document.querySelector('[type=submit]').setAttribute('disabled', 'yes');
})

reservation.addEventListener('submit', (event: Event) => {
    event.preventDefault();
    document.querySelector('#booked').removeAttribute('hidden');
    showFormData();
})
