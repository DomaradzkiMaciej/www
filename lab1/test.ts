import {expect} from 'chai';
import {driver, Key} from 'mocha-webdriver';

const filePath = `file://${process.cwd()}/main.html`

function getDateAltered(alternation: number): string {
    const date = new Date();
    date.setDate(date.getDate() + alternation);

    return date.toISOString().split('T')[0];
}

describe('testing form', function () {

    it('test options in form', async function () {

        this.timeout(20000);
        await driver.get(filePath);

        expect(await driver.find('#from').getText()).to.include('...');
        expect(await driver.find('#from').getText()).to.include('Warszawa');
        expect(await driver.find('#from').getText()).to.include('Rzeszów');
        expect(await driver.find('#from').getText()).to.include('Londyn');
        expect(await driver.find('#from').getText()).to.include('Pekin');
        expect(await driver.find('#from').getText()).to.include('Tokio');
        expect(await driver.find('#from').getText()).to.include('Nowy York');

        expect(await driver.find('#to').getText()).to.include('...');
        expect(await driver.find('#to').getText()).to.include('Warszawa');
        expect(await driver.find('#to').getText()).to.include('Rzeszów');
        expect(await driver.find('#to').getText()).to.include('Londyn');
        expect(await driver.find('#to').getText()).to.include('Pekin');
        expect(await driver.find('#to').getText()).to.include('Tokio');
        expect(await driver.find('#to').getText()).to.include('Nowy York');
    });

    it('submit input should be initially disabled', async function () {

        this.timeout(20000);
        await driver.get(filePath);

        expect(await driver.find('.group input[type=submit]').isEnabled()).to.equal(false);
    });

    it('empty name', async function () {

        this.timeout(20000);
        await driver.get(filePath);

        // empty string
        await driver.find('#name').sendKeys('');
        await driver.find('#surname').sendKeys('Deschain');
        await driver.find('#from').sendKeys(Key.ARROW_DOWN);
        await driver.find('#from').sendKeys(Key.RETURN);
        await driver.find('#to').sendKeys(Key.ARROW_DOWN);
        await driver.find('#to').sendKeys(Key.RETURN);
        await driver.find('#date').doClick();
        await driver.find('#date').sendKeys(getDateAltered(1));

        expect(await driver.find('.group input[type=submit]').isEnabled()).to.equal(false);
    });

    it('empty surname', async function () {

        this.timeout(20000);
        await driver.get(filePath);

        await driver.find('#name').sendKeys('Roland');
        // empty string
        await driver.find('#surname').sendKeys('');
        await driver.find('#from').sendKeys(Key.ARROW_DOWN);
        await driver.find('#from').sendKeys(Key.RETURN);
        await driver.find('#to').sendKeys(Key.ARROW_DOWN);
        await driver.find('#to').sendKeys(Key.RETURN);
        await driver.find('#date').doClick();
        await driver.find('#date').sendKeys(getDateAltered(1));

        expect(await driver.find('.group input[type=submit]').isEnabled()).to.equal(false);
    });

    it('invalid from', async function () {

        this.timeout(20000);
        await driver.get(filePath);

        await driver.find('#name').sendKeys('Roland');
        await driver.find('#surname').sendKeys('Deschain');
        await driver.find('#from').sendKeys(Key.ARROW_DOWN);
        await driver.find('#from').sendKeys(Key.RETURN);
        // not chosen from
        await driver.find('#date').doClick();
        await driver.find('#date').sendKeys(getDateAltered(1));

        expect(await driver.find('.group input[type=submit]').isEnabled()).to.equal(false);
    });

    it('invalid to', async function () {

        this.timeout(20000);
        await driver.get(filePath);

        await driver.find('#name').sendKeys('Roland');
        await driver.find('#surname').sendKeys('Deschain');
        // not chosen to
        await driver.find('#to').sendKeys(Key.ARROW_DOWN);
        await driver.find('#to').sendKeys(Key.RETURN);
        await driver.find('#date').doClick();
        await driver.find('#date').sendKeys(getDateAltered(1));

        expect(await driver.find('.group input[type=submit]').isEnabled()).to.equal(false);
    });

    it('invalid date', async function () {

        this.timeout(20000);
        await driver.get(filePath);

        await driver.find('#name').sendKeys('Roland');
        await driver.find('#surname').sendKeys('Deschain');
        await driver.find('#from').sendKeys(Key.ARROW_DOWN);
        await driver.find('#from').sendKeys(Key.RETURN);
        await driver.find('#to').sendKeys(Key.ARROW_DOWN);
        await driver.find('#to').sendKeys(Key.RETURN);
        await driver.find('#date').doClick();
        await driver.find('#date').sendKeys(getDateAltered(-1));

        expect(await driver.find('.group input[type=submit]').isEnabled()).to.equal(false);
    });

    it('not chosen date', async function () {

        this.timeout(20000);
        await driver.get(filePath);

        await driver.find('#name').sendKeys('Roland');
        await driver.find('#surname').sendKeys('Deschain');
        await driver.find('#from').sendKeys(Key.ARROW_DOWN);
        await driver.find('#from').sendKeys(Key.RETURN);
        await driver.find('#to').sendKeys(Key.ARROW_DOWN);
        await driver.find('#to').sendKeys(Key.RETURN);
        // not chosen date

        expect(await driver.find('.group input[type=submit]').isEnabled()).to.equal(false);
    });

    it('valid input', async function () {

        this.timeout(20000);
        await driver.get(filePath);

        await driver.find('#name').sendKeys('Roland');
        await driver.find('#surname').sendKeys('Deschain');
        await driver.find('#from').sendKeys(Key.ARROW_DOWN);
        await driver.find('#from').sendKeys(Key.RETURN);
        await driver.find('#to').sendKeys(Key.ARROW_DOWN);
        await driver.find('#to').sendKeys(Key.RETURN);
        await driver.find('#date').doClick();
        await driver.find('#date').sendKeys(getDateAltered(0));

        expect(await driver.find('.group input[type=submit]').isEnabled()).to.equal(true);
    });

    it('check shown message', async function () {

        this.timeout(20000);
        await driver.get(filePath);

        const firstCityFromLists = 'Warszawa';
        const name: string = 'Roland';
        const surname: string = 'Deschain';
        const data: string = getDateAltered(0);

        await driver.find('#name').sendKeys(name);
        await driver.find('#surname').sendKeys(surname);
        await driver.find('#from').sendKeys(Key.ARROW_DOWN);
        await driver.find('#from').sendKeys(Key.RETURN);
        await driver.find('#to').sendKeys(Key.ARROW_DOWN);
        await driver.find('#to').sendKeys(Key.RETURN);
        await driver.find('#date').doClick();
        await driver.find('#date').sendKeys(data);

        await driver.find('.group input[type=submit]').doClick();

        expect(await driver.find('#booked').getText()).to.include(name);
        expect(await driver.find('#booked').getText()).to.include(surname);
        expect(await driver.find('#booked').getText()).to.include(firstCityFromLists);
        expect(await driver.find('#booked').getText()).to.include(firstCityFromLists);
        expect(await driver.find('#booked').getText()).to.include(firstCityFromLists);
    });

    it('links should be inactive after sending form', async function () {

        this.timeout(20000);
        await driver.get(filePath);

        await driver.find('#name').sendKeys('Roland');
        await driver.find('#surname').sendKeys('Deschain');
        await driver.find('#from').sendKeys(Key.ARROW_DOWN);
        await driver.find('#from').sendKeys(Key.RETURN);
        await driver.find('#to').sendKeys(Key.ARROW_DOWN);
        await driver.find('#to').sendKeys(Key.RETURN);
        await driver.find('#date').doClick();
        await driver.find('#date').sendKeys(getDateAltered(0));

        await driver.find('.group input[type=submit]').doClick();

        expect(await driver.find('a').doClick()
            .then(() => true, () => false)
        ).to.equal(false);
    });
});
